// alert("Hello!");
	
	// Global variable
	let firstNum1 = 5;
	let secondNum1 = 15;
	let thirdNum1 = 20;
	let fourthNum1 = 50;
	let fifthNum1 = 10
	let radiusNum = 15;
	let piNum = 3.1416;
	let ave1 = 20;
	let ave2 = 40;
	let ave3 = 60;
	let ave4 = 80;
	let score = 38;
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

	function AddFunction(firstNum1, secondNum1) {
		let result = firstNum1 + secondNum1;
		console.log("Displayed sum of " + firstNum1 + " and " + secondNum1);
		console.log(result);
	};
	AddFunction(firstNum1, secondNum1);


	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	function subtractFunction(firstNum1, thirdNum1) {
		let result =  thirdNum1 - firstNum1;
		console.log("Displayed sum of " + thirdNum1 + " and " + firstNum1);
		console.log(result);
	};
	subtractFunction(firstNum1, thirdNum1);

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.

	function multiplicationFunction(fourthNum1, fifthNum1) {
		let result = fourthNum1 * fifthNum1;
		return result;
	};
	let multiplicationResult = multiplicationFunction(fourthNum1, fifthNum1);
	console.log("The product of " + fourthNum1 + " and " + fifthNum1 + ":");
	console.log(multiplicationResult);

	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.

	function divisionFunction(fourthNum1, fifthNum1) {
		let result = fourthNum1 / fifthNum1;
		return result;
	};
	let divisionResult = divisionFunction(fourthNum1, fifthNum1);
	console.log("The quotient of " + fourthNum1 + " and " + fifthNum1 + ":");
	console.log(divisionResult);
	//  	Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.

	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.
	function getAreaOfCircle(radiusNum, piNum) {
		let area = radiusNum * radiusNum * piNum;
		return area;
	};

	
	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.

	let getArea = getAreaOfCircle(radiusNum, piNum);
	console.log("The result of getting the area of a circle with " + radiusNum + " radius:");
	console.log(getArea);

	// Log the value of the circleArea variable in the console.

	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.
	function getTotalAverage(ave1, ave2, ave3, ave4) {
		let sum = ave1 + ave2 + ave3 + ave4;
		let result = sum / 4;
		return result;
	};
	let averageVar = getTotalAverage(ave1, ave2, ave3, ave4);
	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.
	
	console.log("The average of " + ave1 + "," + ave2 + "," + ave3 + " and " + ave4 +"" + ":");
	console.log(averageVar);

	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.
	function checkScorePercentage(score, averageVar) {
		let percentage = (score / averageVar) * 100;
		return percentage;
	}
	let isPassed = checkScorePercentage(score, averageVar);
	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.
	console.log("Is " + score + "/" + averageVar + " a passing score?");
	console.log(isPassed > 75);
